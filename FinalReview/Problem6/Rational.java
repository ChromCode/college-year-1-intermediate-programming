// Rational.java
class Rational{
 int numerator; int denominator; // Fields
 Rational(int n, int d){ // Constructor
 if(d==0) throw new RuntimeException("zero denominator");
 numerator = n; denominator = d;
 }
 Rational plus(Rational Q){
   Rational j = new Rational(1,1);
   j.numerator = (this.denominator * Q.numerator) + (Q.denominator * this.numerator);
   j.denominator = this.denominator * Q.denominator;
   return j; 
 }
 Rational times(Rational Q){
   Rational j = new Rational(1,1);
   j.numerator = this.numerator * Q.numerator;
   j.denominator = this.denominator * Q.denominator;
   return j;
 }
 public String toString(){
    String s = String.valueOf(this.numerator);
    String s1 = String.valueOf(this.denominator);
    return s+"/"+s1; 
 }
 public boolean equals(Object x){
 	Rational k;
    boolean eq = false;

    if( x instanceof Rational ){  
         k = (Rational) x;
         eq = (this.numerator/this.denominator) == (k.numerator/k.denominator);
      }
      return eq;
 }
} // end of class Rational