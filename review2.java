// Problem2.java
class review2{
    public static void main(String[] args){
        int[][] table = { {3, 9, 6, 12},
            {23, -25, 54},
            {0, -12, 10000000, 8, 16} };
        System.out.println(getMax(table)); // prints 54
    }

    static int getMax(int[][] A){
 	    int max = 0;
        for(int i = 0; i < A.length; i++) {
        	int [] second = A[i];
    	    for(int j = 0; j < second.length; j++) {
	    		if(A[i][j] > max) {
			        max = A[i][j];
		        }
    	    }
        }
        return max;
    }
}

// public class ArrayExample {
//     public static void main(String[] args) {
//         int[][] array=
//         {
//             {1},
//             {1, 2, 3},
//             {1, 2, 3, 4, 5},
//             {1, 2}
//         };
//         int x, y;
//         int[] second;

//         for (x = 0; x < array.length; ++x) {
//           second = array[x];
//           for (y = 0; y < second.length; ++y) {
//               System.out.println(x + "," + y + ": " + second[y]);
//           }
//           System.out.println();
//         }
//     }
// }