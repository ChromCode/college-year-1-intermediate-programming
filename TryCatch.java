import java.util.Scanner;

class TryCatch{

   public static void main( String[] args ){

      Scanner sc = new Scanner(System.in);
      boolean defined = true;
      int a, b, c=0; // See what happens if c is not initialized here
                     // Why is it a syntax error?

      System.out.print("Enter dividend and divisor: ");
      a = sc.nextInt();
      b = sc.nextInt();

      // Here is a try-catch statement to perform integer division.  Enter
      // 0 as the divisor to cause an ArithmeticException.
      try{
         c = a/b;
      }catch(ArithmeticException e){
         System.out.println(e.getMessage());
         defined = false;
      }

      System.out.println("Integer quotient = " + (defined?c:"undefined"));
   }
}
