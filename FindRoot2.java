//-----------------------------------------------------------------------------
// FindRoot2.java
// 
// Anoter version of the bisection method, a little closer to the description
// given in the project handout (top of page 3.)
//
//-----------------------------------------------------------------------------

class FindRoot2{

   public static void main(String[] args){

      double a=1.0, b=10.0, tolerance=0.00001;
      double mid=a, width;

      width = b-a;
      while ( width>tolerance ){
         mid = (a+b)/2;
         if( f(a)*f(mid)<0 ){  // sign changes across interval [a, mid]
            b = mid;
         } else {              // sign changes across interval [mid, b] 
            a = mid;
         }
         width = b-a;
      }
      System.out.println("root = " + mid);
   }
   
   static double f(double x){ 
      return (x * x - 2.0); 
   }
   
}