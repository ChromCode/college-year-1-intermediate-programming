//-----------------------------------------------------------------------------
// Queens.java
// Eric Poels
// epoels
// pa5
// Find the solutions to the n-queens problem
//-----------------------------------------------------------------------------

import java.util.Scanner;
class Queens {
   public static void main(String[] args){
      int n = 0;
      boolean verbose = false;
      
      try {
         args[0].equals("-v");
      } catch (ArrayIndexOutOfBoundsException e) {
         Error();
      }
      
      try {
         n = Integer.parseInt(args[args.length-1]);
      } catch (NumberFormatException e) {
         Error();
      }
      if(args[0].equals("-v")) {
         verbose = true;
      }   
      int[] A = new int[n+1];
      for(int i=0;i<=n;i++) {
         A[i] = i;
      }
      int solution = 0; // will show the number of solutions found
      long N = factorial(n);// n factorial 
      int x = 1;// variable used to track permutation
      while(x<=N) {
         nextPermutation(A);
          if(isSolution(A)) {
             solution ++;
          }   
          if(isSolution(A) && verbose) {
             printArray(A);
          }   
         x++;
      }
      System.out.println(n+"-Queens has "+solution+" solutions.");
   }   
   static void nextPermutation(int[] A) {
      //Scanner sc = new Scanner(System.in);
     
         int pivot=0;
         for(int i=A.length-2;i>0;i--) {
            if(A[i] < A[i+1]) {
               pivot = i; //index not value
               break;
            }
         }  
         if(pivot == 0) {
            reverseArray(A,pivot+1);
            return;
         }
      
         int successor = 0;
         for(int i=A.length-1;i>0;i--) {
            if(A[i] > A[pivot]) {
               successor = i; //index not value
               break;
            }
         }
         swap(A, pivot, successor);
         reverseArray(A, pivot+1);
   }
   
   static boolean isSolution(int[] A) {
      for(int i=1; i<=A.length-2; i++) {
         for(int j=i+1; j <=A.length-1; j++) {
            if(A[j]>A[i]) { // watch for negatives
		   if(A[j] - A[i] == j-i) {
			  return false;
		   }
             }
             if(A[i] > A[j]) { // watch for negatives
                if(A[i] - A[j] == j-i) {
                   return false;
                }
             }
          } 
      }
      return true;
   }
      
   static void swap(int[] A, int i, int j){
         int temp = A[i];
         A[i] = A[j];
         A[j] = temp;
      }

   static void reverseArray(int[] A, int i){
      int j = A.length-1;
      while(i<j){
         
         swap(A, i, j);
         i++;
         j--;
      }
   }
    static void printArray(int[] A){
      System.out.print("(");
      for(int i=1; i<A.length; i++){
         if(i == A.length-1) {
            System.out.print(A[i]);
            break;
         }   
         System.out.print(A[i]+", ");
      }
      System.out.print(")");
      System.out.println();
   }
   
   static long factorial(int n){
      long product = 1, i = 1;
      while(i<=n){
         product *= i;
         i++;
      }
      return product;
   }
   static void Error() {
      System.out.println("Usage: Queens [-v] number");
      System.out.println("Option: -v verbose output, print all solutions");
      System.exit(0);
   }
}
