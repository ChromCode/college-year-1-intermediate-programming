// Problem1.java
class Problem1{
 public static void main( String[] args ){
 int a=7, b=2, c;
 double t = 4.4, s = 5.0;
 double x=6.1, y=4.3, z;
 c = fcn1(a, b);
 b = fcn2(x, y);
 z = fcn2(c, b);
 System.out.println("a="+a+", b="+b+", c="+c);
 System.out.println("x="+x+", y="+y+", z="+z);
 System.out.println("bye!");
 System.out.println((int)s);
 }
 static int fcn1(int i, int j){
 int k = (i++) + (++j);
 return k+2;
 }
 static int fcn2(double u, double v){
 return fcn1((int)(u+v), 4 );
 }
 static double fcn2(int r, int s){
 return (double)fcn1(r+s, 7);
 }
}