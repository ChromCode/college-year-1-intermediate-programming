
class Problem2 {
   public static void main(String[] args) {
      int[] A = {1,2,3};
      int[] B = {4,5,6,7};
      
      for(int i=0; i<A.length+B.length; i++) {
         System.out.print(concatenate(A,B)[i] +  " ");
      }
  }
  static int[] concatenate(int[] A, int[] B){
     int[] C = new int[A.length+B.length];
     for(int i=0; i<A.length; i++) {
        C[i] = A[i];
     }
     for(int i=0; i<B.length; i++) {
        C[i+A.length] = B[i];
     }
     return C;
  } 
}