class Problem7{
 public static void main(String[] args){
 int [] list = {3, 9, -164, 113, 23, 1, 54, 6, -4, 2, 27};
 System.out.println(list[getMaxIndex(list)]); // prints 54
 System.out.println(list[getMinIndex(list)]); // prints -25
 }
 static int getMaxIndex(int[] A){
 int max = 0;
 int value = 0;
 for(int i=0; i<A.length-1; i++) {
    if(A[i] > max) {
       max = A[i];
       value = i;
    }
    
 }
    return value;
  
 }
 static int getMinIndex(int[] A){
 int min = 0;
 int value = 0;
for(int i=0; i<A.length-1; i++) {
    if(A[i] <= min) {
       min = A[i];
       value = i;
    }
    
 }
    return value;
 }
}