//-----------------------------------------------------------------------------
// Lawn.java
// Eric Poels
// epoels
// pa2
// 3 guesses to correctly guess a randomly generated number between 1-10
//-----------------------------------------------------------------------------

import java.util.Scanner;

class Guess{
   public static void main(String[] args) {
      double guess1, guess2, guess3, guess;
      Scanner sc = new Scanner(System.in);
      
      int correct = (int)(Math.random() *10) +1; // random number between 1-10
      String win = "You win!";
      String tooLow = "Your guess is too low.";
      String tooHigh = "Your guess is too high.";
      String lose = "You Lose.  The number was "+correct+".";
      
      //System.out.println(correct); //(was used to test accuracy of program)
      
      System.out.println();  
      System.out.println("I'm thinking of an integer in the range 1 to 10. You have three guesses.");
      System.out.println();
      
      for(int i = 1; i <=3; i++) {
         if(i == 1) { System.out.print("Enter your first guess: "); }
         if(i == 2) { System.out.print("Enter your second guess: "); }
         if(i == 3) { System.out.print("Enter your third guess: ");  }
         guess = sc.nextInt();
         if( guess == correct) { System.out.println(win); break; }
         else { System.out.print(guess > correct? tooHigh:tooLow); System.out.println(); }
         if(i == 3) {System.out.println(lose); System.out.println(); }
      }
   }
}
       