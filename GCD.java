//-----------------------------------------------------------------------------
// GCD.java
// Eric Poels
// epoels
// pa3
// determine the Greatest Common Denominator of 2 numbers provided by the user
//-----------------------------------------------------------------------------

import java.util.Scanner;

class GCD{
   public static void main(String[] args) {
      int a, b, r, x, y;
      Scanner sc = new Scanner(System.in);
      
      for(a=0; a<1; a++){ // a is initialized here 
         System.out.print("Enter a positive integer: ");
         while(true){ // seemingly infinite loop
            while( !sc.hasNextInt() ){ // if its not a int
               sc.next(); // discard the non-int
               System.out.print("Please enter a positive integer: "); // ask again
            }
            // at this point we have a int in the input stream, unread

            a = sc.nextInt()-1;  // -1 because at the end of the for loop
            //'a' will have one more than the input value.
            if( a>0 ){  // if it's positive
               break; // get out of the while loop
            }
            // otherwise ask again
            System.out.print("Please enter a positive integer: ");
         }
         // break lands here
      }
      
      for(b=0; b<1; b++){ // b is initialized here
         System.out.print("Enter another positive integer: ");
         while(true){ // seemingly infinite loop
            while( !sc.hasNextInt() ){ // if its not a int
               sc.next(); // discard the non-int
               System.out.print("Please enter a positive integer: "); // ask again
            }
            
            b = sc.nextInt()-1;  // - 1 because at the end of the for loop
            //'b' will have one more than the input value.
            if( b>0 ){  // if it's positive
               break; // get out of the while loop
            }
            // otherwise ask again
            System.out.print("Please enter a positive integer: ");
         }
         // break lands here
      }
      r = a>b? a%b:b%a; //used to define what r is   
      
      x = a; // a changes, so this is used to identify the first value of 'a'
      y = b; // b changes, so this is used to identify the first value of 'b'
      
      while(r>0) { // loop to till the remainder 'r' = '0'
         a = b;
         b = r;
         r = a>b? a%b:b%a; // use the larger integer first
           
      }
      System.out.println("The GCD of "+x+" and "+y+" is " +b);
   }
}