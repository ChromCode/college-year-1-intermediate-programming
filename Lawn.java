//-----------------------------------------------------------------------------
// Lawn.java
// Eric Poels
// epoels
// pa1
// compute hours, minutes, and seconds it takes to mow a lawn based off user input
//-----------------------------------------------------------------------------
import java.util.Scanner;

class Lawn{
   public static void main(String[] args) {
      double seconds, length1, length2, width1, width2, area1, area2, area, rate;
      int h, m, s;
      Scanner sc = new Scanner(System.in);
      
      System.out.print("Enter the length and width of the lot, in feet: ");
      length1 = sc.nextDouble();
      width1 = sc.nextDouble();
      area1 = length1*width1;
      System.out.print("Enter the length and width of the house, in feet: ");
      length2 = sc.nextDouble();
      width2 = sc.nextDouble(); 
      area2 = length2*width2;
      area = area1-area2;
      System.out.println("The lawn area is "+area+" square feet.");
      System.out.print("Enter the mowing rate, in square feet per second: ");
      rate = sc.nextDouble();
      seconds = area/rate;
      
      s = (int) Math.round(seconds); 
      m = s/60;
      s = s%60;
      h = m/60;
      m = m%60;
      System.out.println("The mowing time is "+h+" hour"+(h==1?" ":"s ") +m+ " minute"+(m==1?" ":"s ") +s+" second"+(s==1?" ":"s"));
     
      
   }      
}