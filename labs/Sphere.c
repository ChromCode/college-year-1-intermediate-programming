/* Sphere.c */
#include<stdio.h>
#include<stdlib.h>
#include<math.h>

int main(void){
   const double pi = 3.141592654;
   double r;
   double volume;
   double sArea;
   double rCubed;
   double rSquared;
   
   printf("Enter the sphere of the radius: ");
   scanf("%lf", &r);
   rCubed = pow(r,3);
   rSquared = pow(r,2);
   volume = (4.0/3.0) * pi * rCubed;
   sArea = 4 * pi * rSquared;
   printf("The volume is %f and the surface area is %f.", volume, sArea);
   printf("\n");
   return 0;  
}