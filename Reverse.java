class Reverse{
   
   public static void main(String[] agrs){
      int[] A = {1, 2, 3, 4};
      int[] B;

      printArray(A);
      swap(A,2,3);
      printArray(A);
      reverse1(A,2);
      printArray(A);

      //B = reverse2(A);
      //printArray(B);
      //printArray(A);
   }

   static void printArray(int[] P){
      System.out.print("{");
      for(int i=0; i<P.length; i++){
         System.out.print(P[i]+" ");
      }
      System.out.print("}");
      System.out.println();
   }

   static void swap(int[] Q, int i, int j){
      int temp = Q[i];
      Q[i] = Q[j];
      Q[j] = temp;
   }

   static void reverse1(int[] T, int i){
      int j=T.length-1;
      while(i<j){
         swap(T, i, j);
         i++;
         j--;
      }
   }

   /*static int[] reverse2(int[] S){
      int i, n = S.length;
      int[] R = new int[n];
      for(i=0; i<n; i++){
         R[i] = S[n-1-i];
      }
      return R;
   }*/

}