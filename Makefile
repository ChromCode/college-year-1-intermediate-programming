#------------------------------------------------------------------------------
#   Makefile for pa6
#   Creates an executable jar file ComplexTest
#   Archives both ComplexTest.java and Complex.java 
#------------------------------------------------------------------------------

HelloWorld: HelloWorld.class
	echo Main-class: HelloWorld > Manifest
	jar cvfm HelloWorld Manifest HelloWorld.class
	rm Manifest
	chmod u+x HelloWorld

HelloWorld.class: HelloWorld.java 
	javac -Xlint HelloWorld.java

clean:
	rm -f HelloWorld.class HelloWorld

submit:
	submit cmps011-pt.w15 pa6 Complex.java ComplexTest.java Makefile