//-----------------------------------------------------------------------------
// Complex.java
// Eric Poels
// epoels
//   pa6
//   Fill in the function definitions below.  Write a ComplexTest class to
//   test all your methods. See notes from 3-4-13 for definitions of the
//   complex arithmetic operations.
//-----------------------------------------------------------------------------

class Complex{

   //--------------------------------------------------------------------------
   // Private Data Fields 
   //--------------------------------------------------------------------------
   private double re;
   private double im;
   
   //--------------------------------------------------------------------------
   // Public Constant Fields 
   //--------------------------------------------------------------------------
   public static final Complex ONE = Complex.valueOf(1,0);
   public static final Complex ZERO = Complex.valueOf(0,0);
   public static final Complex I = Complex.valueOf(0,1);

   //--------------------------------------------------------------------------
   // Constructors 
   //--------------------------------------------------------------------------
   Complex(double a, double b){
      this.re = a;
      this.im = b;
   }

   Complex(double a){
      this.re = a;
      this.im = 0;
   }

   Complex(String s){
      // It should accept expressions like "-2+3i", "2-3i", "3", "5i", etc..
      // Throw a NumberFormatException if s cannot be parsed as Complex.
      double[] C = parseComplex(s);
      this.re = C[0];
      this.im = C[1];
   }

   //---------------------------------------------------------------------------
   // Public methods 
   //---------------------------------------------------------------------------

   // Complex arithmetic -------------------------------------------------------

   // copy()
   // Return a new Complex equal to this Complex
   Complex copy(){
      return new Complex(this.re, this.im);
   }
   
   // add()
   // Return a new Complex representing the sum this plus z.
   Complex add(Complex z){
      return new Complex(this.re + z.re, this.im + z.im);
   }
   
   // negate()
   // Return a new Complex representing the negative of this.
   Complex negate(){
      return new Complex(this.re * (-1), this.im * (-1));
   }

   // sub()
   // Return a new Complex representing the difference this minus z.
   Complex sub(Complex z){
      return new Complex(this.re - z.re, this.im - z.im);
   }

   // mult()
   // Return a new Complex representing the product this times z.
   Complex mult(Complex z){
      double x = (this.re * z.re) - (this.im * z.im);
      double y = (this.re * z.im) + (this.im * z.re);
      return new Complex(x,y);
   }

   // recip()
   // Return a new Complex representing the reciprocal of this.
   // Throw an ArithmeticException with appropriate message if 
   // this.equals(Complex.ZERO).
   Complex recip(){
      double x = Math.pow(this.re,2);
      double y = Math.pow(this.im,2);
      if(this.equals(Complex.ZERO)) {
         throw new ArithmeticException(
            "Can't divide by 0");
      }
      return new Complex(this.re /(x+y), (this.im * (-1))/(x+y));
      
   }

   // div()
   // Return a new Complex representing the quotient of this by z.
   // Throw an ArithmeticException with appropriate message if 
   // z.equals(Complex.ZERO).
   Complex div(Complex z){
      double x = (this.re * z.re) + (this.im * z.im);
      double y = (z.re * this.im) - (this.re * z.im);
      double a = z.re *z.re;
      double b = z.im * z.im;
      if(this.equals(Complex.ZERO)) {
         throw new ArithmeticException(
            "Can't divide by 0");
      }
      return new Complex((x)/(a+b), (y)/(a+b));   
   }

   // conj()
   // Return a new Complex representing the conjugate of this Complex.
   Complex conj(){
      return new Complex(this.re, this.im * (-1));
   }
   
   // Re()
   // Return the real part of this.
   double Re(){
      return re;
   }

   // Im()
   // Return the imaginary part of this.
   double Im(){
      return im;
   }

   // abs()
   // Return the modulus of this Complex, i.e. the distance between 
   // points (0, 0) and (re, im).
   double abs(){
      double a = Math.pow(this.re, 2);
      double b = Math.pow(this.im, 2);
      return (Math.sqrt(a+b));
   }

   // arg()
   // Return the argument of this Complex, i.e. the angle this Complex
   // makes with positive real axis.
   double arg(){
      return Math.atan2(im, re);
   }

   // Other functions ---------------------------------------------------------
   
   // parseComplex()
   // Returns a double[] of length 2 containing (real, imaginary) parts
   // of a complex number represented by string argument str.  Throws a
   // NumberFormatException if str cannot be parsed as a complex number.
   private static double[] parseComplex(String str){
      double[] part = new double[2];
      String s = str.trim();
      String NUM = "(\\d+\\.\\d*|\\.?\\d+)";
      String SGN = "[+-]?";
      String OP =  "\\s*[+-]\\s*";
      String I =   "i";
      String OR =  "|";
      String REAL = SGN+NUM;
      String IMAG = SGN+NUM+"?"+I;
      String COMP = REAL+OR+
                    IMAG+OR+
                    REAL+OP+NUM+"?"+I;
      
      if( !s.matches(COMP) ){
         throw new NumberFormatException(
                   "Cannot parse input string \""+s+"\" as Complex");
      }
      s = s.replaceAll("\\s","");     
      if( s.matches(REAL) ){
         part[0] = Double.parseDouble(s);
         part[1] = 0;
      }else if( s.matches(SGN+I) ){
         part[0] = 0;
         part[1] = Double.parseDouble( s.replace( I, "1.0" ) );
      }else if( s.matches(IMAG) ){
         part[0] = 0;
         part[1] = Double.parseDouble( s.replace( I , "" ) );
      }else if( s.matches(REAL+OP+I) ){
         part[0] = Double.parseDouble( s.replaceAll( "("+REAL+")"+OP+".+" , "$1" ) );
         part[1] = Double.parseDouble( s.replaceAll( ".+("+OP+")"+I , "$1"+"1.0" ) );
      }else{   //  s.matches(REAL+OP+NUM+I) 
         part[0] = Double.parseDouble( s.replaceAll( "("+REAL+").+"  , "$1" ) );
         part[1] = Double.parseDouble( s.replaceAll( ".+("+OP+NUM+")"+I , "$1" ) );
      }
      return part;
   }
   
   // toString()
   // Return a String representation of this Complex.
   // The String returned will be readable by the constructor Complex(String s)
   public String toString(){
      String s = String.valueOf(this.re);
      String s1 = String.valueOf(this.im);
      if(this.re >0.0 && this.im >0.0) {
         return s+"+"+s1+"i";
      } 
      if(this.im == 0.0) {
         return s;
      }    
      if(this.re == 0.0) {
         return s1+"i";
      } 
      if(this.re <0 && this.im > 0) {
         return s+"+"+s1+"i";
      } else {
         return s+s1+"i";
      }   
   }

   // equals()
   // Return true iff this and obj have the same real and imaginary parts.
   public boolean equals(Object obj){
      Complex x;
      boolean eq = false;

      if( obj instanceof Complex ){  
         x = (Complex) obj;
         eq = this.equals(x.re) &&
              this.equals(x.im);
      }
      return eq;
   }

   // valueOf()
   // Return a new Complex with real part a and imaginary part b.
   static Complex valueOf(double a, double b){
      return new Complex( a, b);
   }

   // valueOf()
   // Return a new Complex with real part a and imaginary part 0.
   static Complex valueOf(double a){
      return new Complex(a);
   }

   // valueOf()
   // Return a new Complex constructed from s.
   static Complex valueOf(String s){
      return new Complex(s);
   }

}